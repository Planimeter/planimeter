![Planimeter](https://rawgit.com/Planimeter/planimeter/master/app/images/logo_planimeter.svg "Planimeter") [![Build Status](https://travis-ci.org/Planimeter/planimeter.svg)](https://travis-ci.org/Planimeter/planimeter) [![Codacy Badge](https://www.codacy.com/project/badge/b7c25f150b194bbf9181465c050d709b)](https://www.codacy.com/public/andrewmcwatters/planimeter)
===========

Hello. We are Planimeter. This is our landing page.

New Members
===========

If you're a Planimeter engineer or designer and have just joined, please edit
the
[main controller](https://github.com/Planimeter/planimeter/blob/master/app/scripts/controllers/main.js)
and add your info to be listed
[here.](http://www.andrewmcwatters.com/planimeter/) Changes will be
built automatically and deployed, and you can confirm the changes passed 
by checking [Travis CI](https://travis-ci.org/Planimeter/planimeter). Changes
take one to two minutes to be seen live, and are generally built faster than
Travis.
