'use strict';

/**
 * @ngdoc function
 * @name publicApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the publicApp
 */
angular.module('publicApp')
  .controller('MainCtrl', function ($scope, $timeout) {
    /**
     * Below are the team members sorted alphabetically by last name with their
     * name, latlng from Google Maps, and whether or not they're new. Feel free
     * to remove that last property when you don't feel new anymore. It's
     * arbitrary, but gives you a label to stand out on our landing page.
     *
     * Oh and by the way, we generally use our hometown, state, or country
     * latlng, not our address, but that's probably a given.
     *
     * Planimeter engineers and designers are a friendly, skilled group. Meet
     * your colleagues if you haven't already.
     *
     * @andrewmcwatters should have set you up with contributor access to
     * everything you need by now. If he hasn't, bug him.
     *
     * Welcome to Planimeter.
     */

    /**
     * Team members.
     */

    $scope.team = [
      {
        name:   'Andy Garcia',
        latlng: '47.614848,-122.3359058'
      },
      {
        name:   'Davlyn Hafaiedh',
        latlng: '50.501079,4.4764595'
      },
      {
        name:   'Ryan Kingstone',
        latlng: '59.8938549,10.7851165'
      },
      {
        name:   'Andrew McWatters',
        latlng: '33.6054149,-112.125051'
      },
      {
        name:   'Matthias Moninger',
        latlng: '47.6964719,13.3457347'
      },
      {
        name:   'Polkm',
        latlng: '42.3676145,-72.505491'
      },
      {
        name:   'Lachlan Temple',
        latlng: '-39.3080008,176.9160145'
      },
      {
        name:   'Frank van Wattingen',
        latlng: '52.371114,4.905701'
      },
      {
        name:   'Kamil Zmich',
        latlng: '53.381024,-1.472441'
      },
      {
        name:   'Manolis Vrondakis',
        latlng: '53.381024,-1.472441'
      }
    ];

    /**
     * Focused member.
     */

    $scope.focusedMember = $scope.team[0];

    /**
     * Focus on a team member.
     */

    $scope.focus = function(member) {
      $scope.focusedMember = member;

      // HACKHACK: $scope.$apply();
      $timeout(angular.noop);
    };
  });
