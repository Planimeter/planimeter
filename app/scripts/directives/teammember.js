'use strict';

/**
 * @ngdoc directive
 * @name publicApp.directive:teamMember
 * @description
 * # teamMember
 */
angular.module('publicApp')
  .directive('teamMember', function ($window) {
    return {
      restrict: 'C',
      link: function postLink(scope, element, attrs) {
        /**
         * Map loaded.
         */

        var mapLoaded = false;

        /**
         * Map marker.
         */

        var marker = angular.element('<div class="marker-team-member"></div>');
        marker.attr('data-latlng', attrs.latlng);

        /**
        * Handle "mapLoaded" events.
        */

        scope.$on('mapLoaded', function() {
          /**
           * Module dependencies.
           */

          var overlay = $window.overlay;

          // map loaded
          mapLoaded = true;

          /**
           * Add marker.
           */

          angular.element('#map').append(marker);
          overlay.add(marker[0]);
        });

        /**
         * Update marker.
         */

        function update() {
          /**
          * Module dependencies.
          */

          var map    = $window.map;
          var google = $window.google;

          if (element.hasClass('active')) {
            marker.addClass('active');

            if (mapLoaded) {
              var latlng = marker.data('latlng').split(',');
              map.center = new google.maps.LatLng(+latlng[0], +latlng[1]);
              map.panTo(map.center);
            }
          } else {
            marker.removeClass('active');
          }
        }

        /**
         * Watch "class" changes.
         */

        scope.$watch(function() {
          return element.attr('class');
        }, update);
      }
    };
  });
